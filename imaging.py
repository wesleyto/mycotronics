import time, cv2, datetime, os, firebase_handler, threading
from multiprocessing import Queue
from utilities import *

##########################
###  HELPER FUNCTIONS  ###
##########################
def fb_push_log(d, f, l, n):
	if connected and fb.is_connected():
		fb.push_log(d, f, l, n)

def set_stream(var, cfg_path):
	"""
	Connects global variables to firebase streams for real-time updates
		of the variables as they change in the database
	"""
	def handler(message):
		globals()[var] = message['data']
		s = "\t(WEB) Changed '{}' to '{}'".format(cfg_path, message['data'])
		logging_q.put([log_string, logfile, s])
	paths = ['configs', get_name(), cfg_path]
	pathsD = ['configs', 'default', cfg_path]
	temp = fb.get_val(*paths)
	globals()[var] = fb.get_val(*pathsD) if temp == None else temp
	stream = fb.make_stream(handler, *pathsD) if temp == None else fb.make_stream(handler, *paths)

def capture_image(dt, cam_num):
	"""
	Creates an OpenCV2 camera instance, captures frame
		data, and sends it to be processed
	"""
	name = "{}_{}.jpg".format(dt, "top" if cam_num == 1 else "bot")
	camera = cv2.VideoCapture(cam_num - 1)
	if camera == None or not camera.isOpened():
		s = "\n>>> Cannot Access Camera {}".format(cam_num)
		logging_q.put([log_string, logfile, s])
	ret, frame = camera.read()
	if (ret):
		process_image(name, frame, cam_num)
	camera.release()
	move_unmoved()

def process_image(name, frame, cam_num):
	"""
	Takes raw frame data and finds a place to store it,
		either locally, or remotely on firebase
	"""
	fb_dir = "camera_top" if cam_num == 1 else "camera_bottom"
	d = "{}/{}/".format(imgdir, fb_dir)
	cv2.imwrite(d + name, frame)
	logging_q.put([log_string, logfile, "\n>>> Created '{}'".format(name)])
	if connected and fb.is_connected():
		url = fb.push_img(name, d + name, fb_dir)
		logging_q.put([log_string, logfile, "\tPushed '{}' to firebase:\n\t{}".format(name, url)])
		try:
			os.remove(d + name)
			logging_q.put([log_string, logfile, "\tRemoving '{}' from local storage".format(name)])
		except:
			os.rename(d + name, d + "unmoved/" + name)
			logging_q.put([log_string, logfile, "\tCould not remove '{}' from local storage".format(name)])
	else:
		logging_q.put([log_string, logfile, "\tCannot connect to firebase to push {}".format(name)])

def move_unmoved_dir(fb_dir):
	"""
	Recursively explores a directory and finds leftover images
		that weren't pushed to firebase, and tries to push them
	"""
	d = "{}/{}/unmoved/".format(imgdir, fb_dir)
	path = d.format(fb_dir)
	jpgs = [x for x in os.listdir(path) if x[-4:] == '.jpg']
	for jpg in jpgs:
		url = fb.push(jpg, path + jpg, fb_dir)
		logging_q.put([log_string, logfile, "Pushed old file '{}' to firebase:\n\t{}".format(jpg, url)])
		try:
			os.remove(path + jpg)
			logging_q.put([log_string, logfile, "Removing '{}' from local storage".format(jpg)])
		except:
			logging_q.put([log_string, logfile, "\tCould not remove '{}' from local storage".format(jpg)])

def move_unmoved():
	"""
	Goes through the image directories and moves
		any left over images to firebase
	"""
	if connected and fb.is_connected():
		for fb_dir in ['camera_top', 'camera_bottom']:
			move_unmoved_dir(fb_dir)

def check_net():
	"""
	Intermittently checks for an internet connection, should
		be run in the background using a separate thread
	"""
	global connected
	while True:
		connected = check_internet()
		time.sleep(net_check_timeout)

##########################
###   VARIABLE SETUP   ###
##########################

### PATHING SETUP ###
cwd = get_cwd()
imgdir = "{}/images".format(cwd)
logfile = "{}/logs/imaging_{}.txt".format(cwd, datetime_str())

### CAMERA(S) SETUP ###
num_cams = 2
last_pic = 0
pic_freq = 30
width = 640
height = 480

### FIREBASE SETUP ###
timeout = 60
net_check_timeout = 30
connected = False
streams = {
	'log_freq':'logFreq',
	'pic_freq':'picFreq',
	'width':'picWidth',
	'height':'picHeight'
}

### LOGGING SETUP ###
dt_start = datetime_str().replace(" ", "_")
last_log_update = 0
log_freq = 60

### MULTITHREADING SETUP ###
logging_q, capture_q, firebase_q = Queue(), Queue(), Queue()
run_logging_job = make_job_runner(logging_q)
run_capture_job = make_job_runner(capture_q)
run_firebase_job = make_job_runner(firebase_q)
runners = [run_logging_job, run_capture_job, run_firebase_job]
pool = [threading.Thread(target=runner) for runner in runners]
for thread in pool:
	thread.start()


##########################
###   GENERAL SETUP	###
##########################

### MAKE REQUISITE DIRECTORIES ###
make_directories()

### LOG HEADER ###
with open(logfile, 'w') as log:
	print_and_log_header(log, "Logging Images")

### CHECK INTERNET AND ESTABLISH FIREBASE CONNECTION ###
try:
	while timeout > 0 and not check_internet():
		time.sleep(1)
		timeout -= 1
	fb = firebase_handler.FirebaseHandler("images")
except Exception as e:
	logging_q.put([log_string, logfile, str(e)])
threading.Thread(target=check_net).start()

### SETUP FOR STREAMS ###
if connected and fb.is_connected():
	for var in streams:
		set_stream(var, streams[var])
else:
	d = get_configs(fb)
	log_freq, pic_freq, width, height = d['logFreq'], d['picFreq'], d['picWidth'], d['picHeight']

##########################
###	 MAIN LOOP	  ###
##########################
while True:
	curr = time.time()
	# Take some pictures and store them
	try:
		if curr > last_pic + pic_freq:
			last_pic = curr
			dt = datetime_str()
			dt = dt.replace(' ', '_').replace(':', '-')
			for cam_num in range(1, num_cams + 1):
				try:
					capture_q.put([capture_image, dt, cam_num])
				except Exception as e:
					logging_q.put([log_string, logfile, str(e)])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
	# Log Everything that occurred
	try:
		if curr > last_log_update + log_freq:
			last_log_update = curr
			firebase_q.put([fb_push_log, os.path.basename(logfile), logfile, "imaging"])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
