# Mycotronics #
* A Raspberry Pi and Arduino based mycotopia incubator

### Managing ###
* Default server URI: [mycotronics.local:8080](http://mycotronics.local:8080)

### SSH and Screen ###
* The python scripts are run in cron using the `screen` utility (`man screen`)
* One can SSH into the Raspberry pi and monitor these processes from the terminal
* After SSHing into the Raspberry pi, you can view the running processes by entering the command `screen -ls`
* You will see three processes, `incubator`, `server`, and `imaging`
* Run `screen -r <pid/pname>` to reattach to that process
* For example, process `42.server` can be reattached to using `screen -r 42` or `screen -r server`
* To detach from a process and return to the root terminal window, hold down `CTRL` and press `a` then `d`
* Processes that use streams (incubator, imaging) cannot be killed using the normal `CTRL-c` command. To kill a stream process, run the command ``kill `pgrep python` `` in a separate terminal window

### CRONTAB ###
- The Python scripts rely on cron to start them at reboot. Add these lines to the crontab where `path_to` is the directory structure to the files (run `crontab -e`)
```
@reboot screen -S incubator -dm python /home/pi/<path_to>/hybrid_new.py
@reboot screen -S imaging -dm python /home/pi/<path_to>/imaging.py
@reboot screen -S server -dm python /home/pi/<path_to>/server.py
```
