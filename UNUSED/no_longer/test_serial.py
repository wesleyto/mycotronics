#! /usr/bin python3

import serial
import time

ser0 = serial.Serial("/dev/ttyACM0", 9600)
ser0.flushInput()
cmd = 'light:on\n'
ser0.write(cmd.encode())
time.sleep(5)
ser0.write(b'light:off\n')
ser0.close()
