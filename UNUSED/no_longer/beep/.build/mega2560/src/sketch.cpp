#include <Arduino.h>
#include "Time/Time.h"
void setup();
void loop();
void printDigits(int digits);
#line 1 "src/sketch.ino"
//#include "Time/Time.h"

void setup()
{
	Serial.begin(9600);
	setTime(12,0,0,1,1,11);
}

void loop()
{
	Serial.println("I'm sending a message at ");
	Serial.print(hour());
	printDigits(minute());
	printDigits(second());
	Serial.println();
	delay(5000);
}

void printDigits(int digits)
{
	Serial.print(":");
	if(digits < 10)
		Serial.print("0");
	Serial.print(digits);
}
