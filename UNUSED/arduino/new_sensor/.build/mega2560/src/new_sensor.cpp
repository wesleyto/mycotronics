#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
void serialFlush();
void setup(void);
void loop(void);
void relay_init();
void relay_SetStatus(unsigned char status);
#line 1 "src/new_sensor.ino"
//#include <OneWire.h>
//#include <DallasTemperature.h>

#define ONE_WIRE_BUS 5

#define ON 0
#define OFF 1

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

int IN = 42;
int d=100;
float setTemp;
float currentTemp;

float Celcius;

void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
}  

void setup(void)
{
  relay_init();
  Serial.begin(9600);
  sensors.begin();
	setTemp=0.0;
	currentTemp=0.0;
	Celcius=0.0;
	
}

void loop(void)
{ 
	String command="off"; 
	command= Serial.readString();
	serialFlush();
	Serial.println(command);
	if (command=="off")
		relay_SetStatus(OFF);
	if (command=="on")
		relay_SetStatus(ON);
	
}

void relay_init()
{
	pinMode(IN, OUTPUT);
	relay_SetStatus(OFF);
}

void relay_SetStatus(unsigned char status)
{
	digitalWrite(IN, status);
}
