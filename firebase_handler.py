import pyrebase, os, subprocess, datetime, config, json
from utilities import *

class FirebaseHandler:
    def __init__(self, log_add = ""):
        self.user = {'idToken':""}
        d = str(datetime.datetime.now()).split('.')[0]
        dt = d.replace(' ', '_')
        if log_add:
            log_add = "_" + log_add
        self.logfile = "/home/pi/jp/logs/firebase_{}{}.txt".format(dt, log_add)
        with open(self.logfile, 'w') as log:
            print_and_log_header(log, "Connecting To Firebase")
        self.sb = "stanford-boxes.appspot.com"
        db_config = {
            "apiKey":"AIzaSyBNM8ll5KyDln9H5z7NCBPT71DgbLHuMi4",
            "authDomain":"stanford-boxes.firebaseapp.com",
            "databaseURL":"https://stanford-boxes.firebaseio.com",
            "storageBucket":self.sb,
            "serviceAccount":"/home/pi/mycotronics/cloud/stanford-boxes-firebase-adminsdk-3urx2-509a097a0b.json"
        }
        with open(self.logfile, 'a') as log:
            if self.check_internet():
                self.firebase = pyrebase.initialize_app(db_config)
                print_and_log(log, ">>> {}\nConnected to '{}'".format(d, db_config['databaseURL']))
            else:
                self.firebase = None
                print_and_log(log, ">>> {}\nNo Internet Connection Detected (ping 'google.com')".format(d))
       
    def check_internet(self):
        dt = str(datetime.datetime.now()).split('.')[0]
        response = 0
        with open (self.logfile, 'a') as log:
            connected = check_internet()
            if connected:
                print_and_log(log, ">>> {}\nSuccessfully pinged Google.com to establish internet connection".format(dt))
            else:
                print_and_log(log, ">>> {}\nCould not ping Google.com to check internet connection".format(dt))
        return connected

    def is_connected(self):
        return self.firebase != None

    def db(self):
        return self.firebase.database()

    def storage(self):
        return self.firebase.storage().child(get_name())

    def img_db(self, place='images'):
        return self.db().child("data").child(get_name()).child(place)

    def cfg_db(self):
        return self.db().child("configs")

    def log_db(self):
        return self.db().child("data").child(get_name()).child("logs")

    def temp_db(self):
        return self.db().child("data").child(get_name()).child("temperature")

    def hum_db(self):
        return self.db().child("data").child(get_name()).child("humidity")

    def push_img(self, filename, path, place='images'):
        dt = datetime_str().replace(" ", "_")
        self.storage().child("images").child(filename).put(path)
        db = self.db()
        link = "http://storage.googleapis.com/{}/{}/images/{}".format(self.sb, get_name(), filename)
        data = {"image":link}
        self.img_db(place).child(dt).set(data)
        with open(self.logfile, 'a') as log:
            print_and_log(log, ">>> {}\nPushed '{}' to firebase images".format(dt, filename))
            print_and_log(log, "\tURL: {}".format(link))
        return link

    def push_val(self, val, db, s):
        dt = datetime_str().replace(" ", "_")
        data = {"value":val}
        db.child(dt).set(data)
        with open(self.logfile, 'a') as log:
            print_and_log(log, ">>> {}\nPushed '{}' for firebase {}".format(dt, val, s))
    
    def push_temp(self, val):
        db = self.temp_db()
        self.push_val(val, db, "temperatures")

    def push_hum(self, val):
        db = self.hum_db()
        self.push_val(val, db, "humidities")

    def push_set_temp(self, val):
        db = self.cfg_db().child(get_name()).child('setTemperature')
        db.set(val)

    def push_set_humidity(self, val):
        db = self.cfg_db().child(get_name()).child('setHumidity')
        db.set(val)

    def push_log(self, dt, filename, path, subdir=''):
        s = self.storage().child("logs").child(filename)
        s.put(path)
        ldb = self.log_db()
        dt_key = datetime_str().replace(" ", "_")

        verb = "Updated" if self.storage().child(get_name()).child("logs").child(filename).get_url(None) else "Pushed"
        link = "http://storage.googleapis.com/{}/{}/logs/{}".format(self.sb, get_name(), filename)
        data = {"log":link}
        if subdir:
            self.log_db().child(subdir).child(dt).set(data)
        else:
            self.log_db().child(dt).set(data)
        with open(self.logfile, 'a') as log:
            print_and_log(log, ">>> {}\n{} '{}' to firebase logs".format(verb, dt_key, filename))

    def get_config(self, name):
        response = self.cfg_db().child(name).get().val()
        if response:
            return response
        self.cfg_db().child(name).set({"name":name})
        return self.cfg_db().child(name).get().val()

    def push_config(self, key, val):
        self.cfg_db().child(get_name()).child(key).set(val)

    def make_stream(self, handler, *args):
        db = self.db()
        for child in args:
            db = db.child(child)
        return db.stream(handler)

    def get_val(self, *args):
        db = self.db()
        for child in args:
            db = db.child(child)
        return db.get().val()
