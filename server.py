import cv2, web, subprocess, os, math, re
from utilities import *

cwd = get_cwd()
idir = "{}/images".format(cwd)
logdir = "{}/logs".format(cwd)
urls = ('/', 'Index', '/images', 'Img', '/images/([0-9]*)', 'Images', '/image/(.*)', 'Image', '/status/(.*)', 'Status')
navlinkstr = "<a style=\"margin-right:1em;\" href=\"{}\">{}</a>"
linkstr = "<a href=\"{}\">{}</a>"
new_linkstr = "<a href=\"{}\" target=\"_blank\">{}</a>"
header = "<h1>{}</h1><h2>{}{}{}{}</h2>".format(
				navlinkstr.format('/', "Mycotronics"),
				#navlinkstr.format('/images', "Images"),
				navlinkstr.format('/status/incubator', "Incubator"),
				navlinkstr.format('/status/camera', "Camera"),
				navlinkstr.format('/status/imagesDB', "DB (Images)"),
				navlinkstr.format('/status/climateDB', "DB (Climate)")
			)
subheader = "<h3>{}</h3>"
urlre = "http[^ \n']*"

if __name__ == "__main__":
	app = web.application(urls, globals())
	app.run()

# HOME PAGE
class Index:
	def GET(self):
		web.header("Content-Type", "text/html;charset=utf-8")
		return header

# REDIRECT NO SLASH /images
class Img:
	def GET(self):
		raise web.redirect('/images/')

# IMAGES INDEX
class Images:
	per_page = 50
	def GET(self, page):
		if page == '':
			page = 0
		else:
			try:
				page = int(page) - 1
			except:
				page = 0
		start = page * Images.per_page
		end = start + Images.per_page - 1
		html = [header, subheader.format("Images"), "<div>Page:"]
		jpgs = [x for x in os.listdir(idir) if x.split('.')[-1] == 'jpg']
		jpgs.sort(reverse=True)
		num_img = len(jpgs)
		end = min(end, num_img)
		jpgs = jpgs[start:end]
		pages = int(math.ceil(num_img / Images.per_page))
		for i in range(pages + 1):
			html.append('<a style="text-decoration:none;" href="/images/{0}"> [{0}] </a>'.format(i+1))
		html.append("</div><br>")
		for f in jpgs:
			html.append(linkstr.format(f[:-4]))
		web.header("Content-Type", "text/html; charset=utf-8")
		return ''.join(html)

# SINGLE IMAGE RESOURCE
class Image:
	def GET(self, name):
		web.header("Content-Type", "image/jpg")
		return open("{}/{}.jpg".format(idir, name), 'rb').read()

# STATUS PAGE HANDLERS
class Status:
	def GET(self, name):
		item = 'imaging' if name == 'camera' else ('images' if name == 'imagesDB' else ('incubator' if name == "climateDB" else 'controller'))
		split = '>>> '
		num_outs = 30
		web.header("Content-Type", "text/html; charset=utf-8")
		logs = [x for x in os.listdir(logdir) if item in x]
		logs.sort()
		log = subprocess.check_output(["cat", "{}/{}".format(logdir, logs[-1])])
		log = log.rsplit(split, num_outs);
		log = log[-num_outs:]
		log.reverse()
		html = [
				header,
				subheader.format("{} Status".format(name.capitalize()))
			]
		for statement in log:
			if statement:
				matches = re.findall(urlre, statement)
				for match in matches:
					statement = statement.replace(match, new_linkstr.format(match, match))
				html.append("<pre>{}</pre>".format(statement))
		return ''.join(html)
