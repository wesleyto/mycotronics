import datetime, subprocess, os, firebase_handler, json, random

##########################
### LOGGING FUNCTIONS  ###
##########################
def print_and_log(log, s):
	"""
	Writes a string to the specified log, then prints it to standard output
	"""
	log.write(s)
	log.write('\n')
	print(s)

def print_and_log_header(log, s):
	"""
	Creates a header from a string and sends it to print_and_log()
	"""
	print_and_log(log, make_header(s))

def log_string(logfile, s):
	"""
	Wrapper for print_and_log() to allow for easier threaded logging
	"""
	with open(logfile, 'a') as log:
		print_and_log(log, s)

def log_header(logfile, s):
	"""
	Wrapper for print_and_log_header() to allow for easier threaded logging
	"""
	with open(logfile, 'a') as log:
		print_and_log_header(log, s)

def make_header(s):
	"""
	Returns a 3-line string of width 42, withthe input
		centered and surrounded by '#' characters
	"""
	return "#" * 42 + "\n{:^42}\n".format(s) + "#" * 42

##########################
###   MATH FUNCTIONS   ###
##########################
def clamp(val, mx, mn):
	"""
	Sets a hard min and max on the input value
	"""
	if val > mx:
		return mx
	if val < mn:
		return mn
	return val

def scale_byte(val, max_val):
	"""
	Scales a 0-255 value to 0-max_val
	Input is assumed to be within unsigned byte range
	"""
	return int(val * (max_val / 255.0))

def avg_f(arr):
	"""
	Returns the average of an array (as a float)
	"""
	return sum(arr) / float(len(arr))

##########################
###   COMM FUNCTIONS   ###
##########################
def get_readings(line):
	"""
	Parse a line of data as read from an arduino and return it as a tuple
	"""
	t_delimiter = "Temp"
	h_delimiter = "Humidity"
	t_start = line.index(t_delimiter)
	h_start = line.index(h_delimiter)
	t = line[t_start + len(t_delimiter):h_start]
	h = line[h_start + len(h_delimiter):]
	return (float(t), float(h))

def check_internet():
	"""
	Pings 'google.com' once to check for an internet connection
	"""
	try:
		subprocess.check_output(['ping', '-c', '1', 'google.com'])
		return True
	except:
		return False

##########################
###   PATH FUNCTIONS   ###
##########################
def datetime_str():
	"""
	Returns the current datetime as a string
	"""
	return str(datetime.datetime.now()).split('.')[0]

def get_cwd():
	"""
	Returns the project's root directory
	"""
	return os.path.dirname(os.path.realpath(__file__))

def make_directories():
	"""
	Creates (in the root directory) the subdirectories 
		necessary for imaging and logging storage
	"""
	cwd = get_cwd()
	paths = [
		"{}/logs".format(cwd),
		"{}/images".format(cwd),
		"{}/images/camera_top".format(cwd),
		"{}/images/camera_bottom".format(cwd),
		"{}/images/camera_top/unmoved".format(cwd),
		"{}/images/camera_bottom/unmoved".format(cwd)
	]
	for path in paths:
		if not os.path.isdir(path):
			os.mkdir(path)

def random_str(length=10):
	"""
	Generates a random alphanumeric string of given length (defaults to 10)
	"""
	l = [x for x in range(65, 91)]
	l += [x for x in range(48, 58)]
	l += [x for x in range(97, 123)]
	return "".join([chr(l[random.randint(0, len(l) - 1)]) for i in range(length)])

##########################
###  CONFIG FUNCTIONS  ###
##########################
def get_name():
	"""
	Returns the name of the mycotron as it appears in the config
	"""
	cfg_path = "{}/config.json".format(get_cwd())
	if os.path.isfile(cfg_path):
		with open(cfg_path, 'r') as cfg:
			js = json.loads(cfg.read())
			return js.get("name")

def store_default_config(fb):
	"""
	Stores a local copy of the remote default configuration
		as it was at mycotron start-up
	"""
	if fb.is_connected():
		cfg_path = "{}/defaults.json".format(get_cwd())
		with open(cfg_path, 'w') as cfg:
			d = fb.get_config("default")
			json.dump(d, cfg)

def get_configs(fb):
	"""
	Tries to get machine specific configuration from firebase
	Failing that, tries to get the default configuration from firebase
	Failing that, tries to get the stored default configuration from local storage
	Failing that, returns a series of hard-coded defaults
	"""
	cfg_path = "{}/config.json".format(get_cwd())
	def_path = "{}/defaults.json".format(get_cwd())
	d = {}
	if fb.is_connected():
		d = fb.get_config(get_name())
	elif os.path.isfile(cfg_path):
		with open(cfg_path, 'r') as cfg:
			d = json.load(cfg.read())
	if fb.is_connected():
		defaults = fb.get_config("default")
	else:
		defaults = {
			"avgReadings": 10,
			"lightB": 90,
			"lightG": 110,
			"lightR": 128,
			"logFreq": 60,
			"picFreq": 30,
			"picHeight": 480,
			"picWidth": 640,
			"lightMaxR":255,
			"lightMaxG":255,
			"lightMaxB":255,
			"setTemperature":22,
			"setHumidity":40
		}
	new_d = {}
	for k in defaults:
		new_d[k] = d.get(k, defaults[k])
	return new_d

def merge_config(fb):
	"""
	Hack for web-app
	Takes all values present in the default firebase config but
		not present in the box-specific firebase config and
		copies them over to the box-specific config
	"""
	if fb.is_connected():
		existing = fb.get_config(get_name())
		defaults = fb.get_config('default')
		for k in defaults:
			if k not in existing:
				fb.push_config(k, defaults[k])

def make_config():
	"""
	Creates a local box-specific configuration file
	"""
	cwd = get_cwd()
	cfg_path = "{}/config.json".format(cwd)
	if not os.path.isfile(cfg_path):
		name = "myco-" + random_str()
		with open(cfg_path, 'w') as cfg:
			d = dict()
			d["name"] = name
			json.dump(d, cfg)

##########################
###  THREAD FUNCTIONS  ###
##########################
def make_job_runner(q):
	def runner():
		while True:
			if not q.empty():
				command = q.get()
				f = command.pop(0)
				f(*command)
	return runner