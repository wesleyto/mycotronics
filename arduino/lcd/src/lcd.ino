#include <TouchScreen.h>
#include <string.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0
#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// Pins for the LCD Shield
#define YP A3 // must be analog
#define XM A2 // must be analog
#define YM 9  // digital or analog pin
#define XP 8  // digital or analog pin

#define MINPRESSURE 1
#define MAXPRESSURE 1000

// Calibration mins and max for raw data when touching edges of screen
#define TS_MINX 210
#define TS_MINY 210
#define TS_MAXX 915
#define TS_MAXY 910

// Values for clamping set temperature and humidity settings
#define MAXT 60
#define MINT 15
#define MAXH 95
#define MINH 5

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define ON   0
#define OFF  1
#define BUFF_SIZE 32

Adafruit_TFTLCD 	tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
TouchScreen		ts = TouchScreen(XP, YP, XM, YM, 300);
enum			BtnPress {temp_u, temp_d, hum_u, hum_d};
static const int	RPI_PIN = 23;
long			lastTouch = 0;
long			lastUpdate = 0;
static const int	touchDelay = 150;
static const int	updateDelay = 250;
int			buttonState;
int			setTemp;
double			currentTemp;
int			setHumidity;
double			currentHumidity;
char			buf[BUFF_SIZE];
static const int	cursorX = 125;
static const int	indent = 10;
bool			demoMode;

void	emptySerial()
{
	while (Serial.available() > 0)
		Serial.read();
}

void	setup()
{
	buttonState = -1;
	setTemp = 30;
	currentTemp = 0.0;
	setHumidity = 50;
	currentHumidity = 0;
	demoMode = false;
	tft.reset();
	uint16_t identifier = tft.readID();
	tft.begin(identifier);
	tft.fillScreen(BLACK);
	tft.setRotation(3);
	tft.setTextSize(2);
	drawButtons();
	drawTemp();
	drawHumidity();
	tft.drawFastHLine(0, 100, 325, WHITE);
	Serial.begin(9600);
	Serial.println("Display");
	pinMode(RPI_PIN, OUTPUT);

}

void	loop()
{
	TSPoint p = ts.getPoint();
	pinMode(XM, OUTPUT);
	pinMode(YP, OUTPUT);
    digitalWrite(RPI_PIN, LOW);
	long curr = millis();
	if (curr > (lastTouch + touchDelay) && p.z > MINPRESSURE && p.z < MAXPRESSURE)
	{
		lastTouch = curr;
		int y = tft.height() - (map(p.x, TS_MINX, TS_MAXX, 0, tft.height()));
		int x = tft.width() - (map(p.y, TS_MINY, TS_MAXY, 0, tft.width()));
		if (x >= 10 && x <= 60 && y >= 215 && y <= 245)
			buttonState = temp_u;
		else if (x >= 100 && x <= 150 && y >= 215 && y <= 245)
			buttonState = temp_d;
		else if (x >= 10 && x <= 60 && y >= 80 && y <= 110)
			buttonState = hum_u;
		else if (x >= 100 && x <= 150 && y >= 80 && y <= 110)
			buttonState = hum_d;
		switch (buttonState)
		{
			case temp_u:
				increaseSetTemperature();
				break;
			case temp_d:
				decreaseSetTemperature();
				break;
			case hum_u:
				increaseSetHumidity();
				break;
			case hum_d:
				decreaseSetHumidity();
				break;
		}
		drawInput(10);
		buttonState = -1;
	}

	if (curr > (lastUpdate + updateDelay) && Serial.available() > 0)
	{
		lastUpdate = curr;
		int n = 0;
		while (Serial.available() > BUFF_SIZE)
		{
			n = Serial.readBytesUntil('@', buf, BUFF_SIZE - 1);
			buf[n] = '\0';
		}
		debugPrint(buf);
		char *s = strchr(buf, '#');
		if (s)
		{
			s++;
			char option = *s;
			s++;
			if (*s == 'D')
				demoMode = true;
			else
				demoMode = false;
			s++;
			currentTemp = atof(s);
			s = strchr(s, ' ') + 1;
			currentHumidity = atof(s);
			if (option == '2')
			{
				s = strchr(s, ' ') + 1;
				setTemp = atoi(s);
				drawSetTemperature();
				s = strchr(s, ' ') + 1;
				setHumidity = atoi(s);
				drawSetHumidity();
			}
			if (demoMode)
			{
				drawDemo();
			}
			else
			{
				drawCurrentTemperature();
				drawCurrentHumidity();
			}
		}
	}
}

void	interrupt_rpi()
{
    digitalWrite(RPI_PIN, HIGH);
}

void	send_to_rpi(int t, int h)
{
	Serial.println("DisplayTemp" + String(t) + "Humidity" + String(h));
	Serial.flush();
	interrupt_rpi();
}

float	clamp(float val, float mn, float mx)
{
	if (val < mn)
		return mn;
	if (val > mx)
		return mx;
	return val;
}

void	debugPrint(char *s)
{
	tft.fillRect(0, 75, 400, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(0, 75);
	tft.print(s);
}

void	drawSetTemperature()
{
	tft.fillRect(cursorX, 25, 200 - cursorX, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(cursorX, 25);
	tft.print(setTemp);
	tft.println(" C");
}

void	drawCurrentTemperature()
{
	tft.fillRect(cursorX, 50, 300 - cursorX, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(cursorX, 50);
	tft.print(currentTemp);
	tft.println(" C");
}

void	drawTemp()
{
	tft.fillRect(0, 0, 200, 100, BLACK);
	tft.setTextColor(WHITE);
	tft.setCursor(0, 0);
	tft.println("Temperature");
	tft.setTextColor(YELLOW);
	tft.setCursor(indent, 25);
	tft.print("Target:  ");
	tft.setCursor(indent, 50);
	tft.print("Current: ");
	drawSetTemperature();
	drawCurrentTemperature();
}

void	drawSetHumidity()
{
	tft.fillRect(cursorX, 150, 200 - cursorX, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(cursorX, 150);
	tft.print(setHumidity);
	tft.print("%");
}

void	drawDemo()
{
	tft.fillRect(cursorX, 50, 300 - cursorX, 25, BLACK);
	tft.fillRect(cursorX, 175, 300 - cursorX, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(cursorX, 50);
	tft.print("DEMO MODE");
	tft.setCursor(cursorX, 175);
	tft.print("DEMO MODE");
}

void	drawCurrentHumidity()
{
	tft.fillRect(cursorX, 175, 300 - cursorX, 25, BLACK);
	tft.setTextColor(YELLOW);
	tft.setCursor(cursorX, 175);
	tft.print(currentHumidity);
	tft.print("%");
}

void	drawHumidity()
{
	tft.fillRect(0, 100, 200, 100, BLACK);
	tft.setTextColor(WHITE);
	tft.setCursor(0, 125);
	tft.println("Humidity");
	tft.setTextColor(YELLOW);
	tft.setCursor(indent, 150);
	tft.print("Target:  ");
	tft.setCursor(indent, 175);
	tft.print("Current: ");
	drawSetHumidity();
	drawCurrentHumidity();
}

void	drawButtons()
{
 	tft.fillTriangle(210, 35, 230, 25, 230, 45, RED);
	tft.fillTriangle(210, 160, 230, 150, 230, 170, RED);
	tft.fillTriangle(280, 45, 280, 25, 300, 35, GREEN);
	tft.fillTriangle(280, 170, 280, 150, 300, 160, GREEN);
}

void	drawInput(int n)
{
	tft.fillRect(0, 75, 300, 25, BLACK);
	tft.setTextColor(BLUE);
	tft.setCursor(75, 75);
	if (n == 0)
		tft.println("Temp UP");
	else if (n == 1)
		tft.println("Temp DOWN");
	else if (n == 2)
		tft.println("Hum UP");
	else if (n == 3)
		tft.println("Hum DOWN");
}

void	increaseSetTemperature()
{
	//int tempTemp = clamp(setTemp + 1, MINT, MAXT);
	drawInput(0);
	send_to_rpi(setTemp + 1, setHumidity);
}

void	decreaseSetTemperature()
{
	//int tempTemp = clamp(setTemp - 1, MINT, MAXT);
	drawInput(1);
	send_to_rpi(setTemp - 1, setHumidity);
}

void	increaseSetHumidity()
{
	//int tempHumidity = clamp(setHumidity + 1, MINH, MAXH);
	drawInput(2);
	send_to_rpi(setTemp, setHumidity + 5);
}

void	decreaseSetHumidity()
{
	//int tempHumidity = clamp(setHumidity - 1, MINH, MAXH);
	drawInput(3);
	send_to_rpi(setTemp, setHumidity - 5);
}

