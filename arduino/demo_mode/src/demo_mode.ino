
#include <Adafruit_NeoPixel.h>
#define LED_COUNT 60
#define LEDPIN 8
#define H_PIN 9
#define ON 1
#define OFF 0

Adafruit_NeoPixel           leds = Adafruit_NeoPixel(LED_COUNT, LEDPIN, NEO_GRB + NEO_KHZ800);
static const unsigned char  r_i = 6;
static const unsigned char  g_i = 4;
static const unsigned char  b_i = 3;
static const unsigned char  r_max = (255 / r_i) * r_i;
static const unsigned char  g_max = (255 / r_i) * g_i;
static const unsigned char  b_max = (255 / r_i) * b_i;
static const unsigned int   hMinMs = 15000; // 15 seconds
static const unsigned int   hMaxMs = 45000; // 45 seconds
unsigned char               humStatus = ON;
unsigned char               set = 0;
unsigned char               r = 0;
unsigned char               g = 0;
unsigned char               b = 0;
long                        lastL = 0;
long                        lastH = 0;
int                         hDelay = random(hMinMs, hMaxMs);
int                         pulseDir = 1;
int                         lDelay = 40;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(H_PIN, OUTPUT);
  leds.begin();
}

bool pulseRGB(bool rb, bool gb, bool bb)
{
    for (int i = 0; i < LED_COUNT; i++)
      leds.setPixelColor(i, r, g, b);
    if (rb)
      r += r_i * pulseDir;
    if (gb)
      g += g_i * pulseDir;
    if (bb)
      b += b_i * pulseDir;
    if (
        (rb && (r == r_max || r == 0)) ||
        (gb && (g == g_max || g == 0)) ||
        (bb && (b == b_max || b == 0))
      )
      pulseDir *= -1;
    leds.show();
    if ((rb && r == 0) || (gb && g == 0) || (bb && b == 0))
      return true;
    return false;
}

void loop() {
  // put your main code here, to run repeatedly:
  long curr = millis();
  if (curr > lastH + hDelay)
   {
    lastH = curr;
    hDelay = random(hMinMs, hMaxMs);
    digitalWrite(H_PIN, humStatus);
    humStatus = humStatus == ON ? OFF : ON;
   }
  if (lastL + lDelay < curr)
  {
    if (set == 0)
    {
      pulseRGB(true, true, true);
      //if (pulseRGB(true, true, true)) //white
        //set++;
    }
    else if (set == 1)
    {
      if (pulseRGB(true, false, false)) //red
        set++;
    }
    else if (set == 2)
    {
      if (pulseRGB(true, true, false)) // yellow
        set++;
    }
    else if (set == 3)
    {
      if (pulseRGB(false, true, false)) // greeen
        set++;
    }
    else if (set == 4)
    {
      if (pulseRGB(false, true, true)) // aqua blue
        set++;
    }
    else if (set == 5)
    {
      if (pulseRGB(false, false, true)) // blue
        set++;
    }
    else if (set == 6)
    {
      if (pulseRGB(true, false, true)) // purple
        set = 0;
    }
    lastL = curr;
  }
}
