#include <OneWire.h>
#include <DallasTemperature.h>
#include <dht11.h>
#include <Adafruit_NeoPixel.h>

#define ONE_WIRE_BUS 5
#define DHT11PIN 2
#define LEDPIN 8
#define LED_COUNT 60
#define T_IN 42
#define H_IN 43
#define D_L_IN 44
#define D_H_IN 45
#define ON 0
#define OFF 1

dht11			DHT11;
OneWire			oneWire(ONE_WIRE_BUS);
DallasTemperature	sensors(&oneWire);
Adafruit_NeoPixel	leds = Adafruit_NeoPixel(LED_COUNT, LEDPIN, NEO_GRB + NEO_KHZ800);

long			lastT = 0;
long			lastH = 0;
long			lastL = 0;
const static int	hDelay = 2000;
const static int	tDelay = 500;
const static int	lDelay = 3000;
float			celcius;
float			humidity;
unsigned char		r = 0xFF;
unsigned char		g = 0xDF;
unsigned char		b = 0xAF;

void	emptySerial()
{
	while (Serial.available() > 0)
		Serial.read();
}

void	printOut(float t, float h)
{
	Serial.println("Temp" +
		String((int)t) + "." +
		String((int)(100 * (t - (int)t))) +
		"Humidity" +
		String((int)h) + "." +
		String((int)(100 * (h - (int)h)))
	);
	Serial.flush();
}

void	updateReadings()
{
	long curr = millis();
	
	if (curr > lastT + tDelay)
	{
		sensors.requestTemperatures();
		celcius = sensors.getTempCByIndex(0);
		lastT = curr;
	}
	if (curr > lastH + hDelay)
	{
		DHT11.read(DHT11PIN);
		humidity = DHT11.humidity;
		lastH = curr;
	}
	if (curr > lastL + lDelay)
	{
		lastL = curr;
		for (int i = 0; i < LED_COUNT; i++)
			leds.setPixelColor(i, r, g, b);
		leds.show();
	}
}

void	setRelays()
{
	String d_command = "d0";
	String t_command = "t0";
	String h_command = "h0";
	if (Serial.available() > 0)
	{
		char buf[4];
		buf[3] = '\0';
		t_command = Serial.readString();
		d_command = t_command.substring(0, 2);
		h_command = t_command.substring(4, 6);
		t_command.substring(9, 11).toCharArray(buf, 3);
		r = (unsigned char)strtoul(buf, NULL, 16);
		t_command.substring(11, 13).toCharArray(buf, 3);
		g = (unsigned char)strtoul(buf, NULL, 16);
		t_command.substring(13, 15).toCharArray(buf, 3);
		b = (unsigned char)strtoul(buf, NULL, 16);
		t_command = t_command.substring(2, 4);
	}
	emptySerial();
	if (d_command == "d0")
		d_relay_set(OFF);
	else if (d_command == "d1")
		d_relay_set(ON);
	if (t_command == "t0")
		t_relay_set(OFF);
	else if (t_command == "t1")
		t_relay_set(ON);
	if (h_command == "h0")
		h_relay_set(OFF);
	else if (h_command == "h1")
		h_relay_set(ON);
}

void	setup(void)
{
	devicesInit();
	Serial.begin(9600);
	sensors.begin();
	leds.begin();
	celcius = 100.0;
	humidity = 100.0;
	Serial.flush();
	printOut(celcius, humidity);
}

void	loop(void)
{ 
	updateReadings();
	setRelays();
	printOut(celcius, humidity);
}

void	devicesInit()
{
	pinMode(T_IN, OUTPUT);
	pinMode(H_IN, OUTPUT);
	pinMode(D_L_IN, OUTPUT);
	pinMode(D_H_IN, OUTPUT);
	t_relay_set(OFF);
	h_relay_set(OFF);
}

void	t_relay_set(unsigned char status)
{
	digitalWrite(T_IN, status);
}

void	h_relay_set(unsigned char status)
{
	digitalWrite(H_IN, status);
}

void	d_relay_set(unsigned char status)
{
	digitalWrite(D_H_IN, status);
	digitalWrite(D_L_IN, status);
}
