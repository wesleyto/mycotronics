import serial, glob, datetime, json, os, time, firebase_handler, threading
from utilities import *
from multiprocessing import Queue
import RPi.GPIO as GPIO

##########################
###  HELPER FUNCTIONS  ###
##########################

def fb_push_th(t, h):
	if connected and fb.is_connected():
		fb.push_temp(t)
		fb.push_hum(h)

def fb_push_set_th(t, h):
	if connected and fb.is_connected():
		fb.push_set_temp(t)
		fb.push_set_humidity(h)

def fb_push_log(d, f, l, n):
	if connected and fb.is_connected():
		fb.push_log(d, f, l, n)

def display_interrupt(channel):
	"""
	Gets the readings from the display, stores it,
		then pushes the data to firebase, if possible
	"""
	global set_temp, set_humidity, changed
	set_temp, set_humidity = get_readings(tft_arduino.readline())
	set_temp = clamp(set_temp, max_t, min_t)
	set_humidity = clamp(set_humidity, max_h, min_h)
	tft_arduino.flushInput()
	changed = True
	print "\nINTERRUPTED on channel {} {}C {}%\n".format(channel, set_temp, set_humidity)
	firebase_q.put([fb_push_set_th, set_temp, set_humidity])

def send_to_display(t, h):
	"""
	Sends temp and hum information to the display in a specified format
	Also sends demo mode information, if it's available
	"""
	global changed
	d = "d"
	if demo_mode:
		d = "D"
	if changed:
		tft_arduino.write("#2{}{:.2f} {:.2f} {:.2f} {:.2f}@".format(d, t, h, set_temp, set_humidity))
	else:
		tft_arduino.write("#1{}{:.2f} {:.2f}@".format(d, t, h))   
	changed = False

def send_to_sensor(dc, tc, hc, color):
	sensor_arduino.write("{}{}{}{}".format(dc, tc, hc, color))

def set_stream(var, cfg_path):
	"""
	Connects global variables to firebase streams for real-time updates
		of the variables as they change in the database
	"""
	def handler(message):
		global changed, set_temp, set_humidity
		globals()[var] = message['data']
		if var in ['set_temp', 'set_humidity']:
			changed = True
			set_temp = clamp(set_temp, max_t, min_t)
			set_humidity = clamp(set_humidity, max_h, min_h)
		s = "\t(WEB) Changed '{}' to '{}'".format(cfg_path, message['data'])
		logging_q.put([log_string, logfile, s])
	paths = ['configs', get_name(), cfg_path]
	pathsD = ['configs', 'default', cfg_path]
	temp = fb.get_val(*paths)
	globals()[var] = fb.get_val(*pathsD) if temp == None else temp
	stream = fb.make_stream(handler, *pathsD) if temp == None else fb.make_stream(handler, *paths)

def average_th(t, h):
	"""
	Adds the input values to the respective lists,
		truncates the list if necessary,
		then returns the averages of both lists as a tuple
	"""
	global t_arr, h_arr
	t_arr.append(t)
	h_arr.append(h)
	t_arr = t_arr[-avg_readings:]
	h_arr = h_arr[-avg_readings:]
	return (avg_f(t_arr), avg_f(h_arr))

def check_net():
	"""
	Intermittently checks for an internet connection, should
		be run in the background using a separate thread
	"""
	global connected
	while True:
		connected = check_internet()
		time.sleep(net_check_timeout)

##########################
###   VARIABLE SETUP   ###
##########################

### INTERRUPT SETUP ###
GPIO.setmode(GPIO.BCM)
GPIOIN = 23
GPIO.setup(GPIOIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # input from display
GPIO.add_event_detect(GPIOIN, GPIO.RISING, callback=display_interrupt, bouncetime=300)

### PORTS SETUP ###
num_ports = 8
ports = ['/dev/ttyUSB{}'.format(i) for i in range(num_ports)]
for i in range(num_ports):
	try:
		ports[i] = serial.Serial(ports[i], 9600)
	except:
		ports[i] = None
ports = filter(lambda x: x != None, ports)
sensor_arduino = tft_arduino = None
t_handshake = "Temp"
d_handshake = "Display"

### LOGGING SETUP ###
dt = datetime_str().replace(' ', '_')
dt_start = dt
cwd = get_cwd()
logfile = "{}/logs/controller_{}.txt".format(cwd, dt)
outstr = """
>>> {}
\tTEMP
\t\tCurr:\t{:.2f} C
\t\tMean:\t{:.2f} C
\t\tSet:\t{:.2f} C
\tHUMI
\t\tCurr:\t{:.2f}%
\t\tMean:\t{:.2f}%
\t\tSet:\t{:.2f}%
\tLIGHT
\t\tRed:\t{}
\t\tGreen:\t{}
\t\tBlue:\t{}
\tDEMO:\t{}
\tHEAT:\t{}
\tHUMI:\t{}"""
last_log_update = 0
log_freq = 60
last_out = 0
out_freq = 2

### LIGHTING SETUP ###
r = 0xFF
g = 0xDF
b = 0x9F
max_r = 0xFF
max_g = 0xFF
max_b = 0xFF

### T & H SETUP ###
max_t, min_t = 60, 15
max_h, min_h = 95, 5
t_fudge = 0.25 # compensate for no PID
h_fudge = 1.0 # ditto
set_temp = 0
set_humidity = 0
avg_readings = 7
t_arr, h_arr = [], []

### FIREBASE SETUP ###
timeout = 30
net_check_timeout = 30
push_freq = 1
last_push = 0
demo_mode = False
connected = False
streams = {
	'max_r':'lightMaxR',
	'max_g':'lightMaxG',
	'max_b':'lightMaxB',
	'r':'lightR',
	'g':'lightG',
	'b':'lightB',
	'log_freq':'logFreq',
	'avg_readings':'avgReadings',
	'set_temp':'setTemperature',
	'set_humidity':'setHumidity',
	'demo_mode':'demoMode',
	'push_freq':'pushFreq'
}

### DISPLAY COMMS SETUP ###
last_display = 0
display_freq = 0.5
changed = True

### MULTITHREADING SETUP ###
sensor_q, logging_q, display_q, firebase_q = Queue(), Queue(), Queue(), Queue()
run_sensor_job, run_logging_job = make_job_runner(sensor_q), make_job_runner(logging_q)
run_display_job, run_firebase_job = make_job_runner(display_q), make_job_runner(firebase_q)
runners = [run_sensor_job, run_logging_job, run_display_job, run_firebase_job]
pool = [threading.Thread(target=runner) for runner in runners]
for thread in pool:
	thread.start()

##########################
###   GENERAL SETUP	###
##########################

### MAKE REQUISITE DIRECTORIES ###
make_directories()

### LOG HEADER ###
with open(logfile, 'w') as log:
	print_and_log_header(log, "Starting Incubator")
	print_and_log_header(log, get_name())

### CHECK INTERNET AND ESTABLISH FIREBASE CONNECTION ###
try:
	while timeout > 0 and not check_internet():
		time.sleep(1)
		timeout -= 1
	fb = firebase_handler.FirebaseHandler("incubator")
except Exception as e:
	logging_q.put([log_string, logfile, str(e)])
threading.Thread(target=check_net).start()

### SETUP 2 FOR THINGS THAT MAY NEED INTERNET CONNECTION ###
store_default_config(fb)
make_config()
merge_config(fb)

### HANDSHAKE PROCEDURE ###
while not (sensor_arduino and tft_arduino):
	logging_q.put([log_header, logfile, "HANDSHAKE"])
	for ser in ports:
		ser_out = ser.readline()
		if t_handshake in ser_out:
			sensor_arduino = ser
			s = "Sensor\t'{}'".format(ser.port)
			logging_q.put([log_header, logfile, s])
		elif d_handshake in ser_out:
			tft_arduino = ser
			s = "Display\t'{}'".format(ser.port)
			logging_q.put([log_header, logfile, s])

### SETUP FOR STREAMS AND READOUTS ###
if connected and fb.is_connected():
	for var in streams:
		set_stream(var, streams[var])
else:
	d = get_configs(fb)
	avg_readings = d['avgReadings']
temp, humidity = get_readings(sensor_arduino.readline())
t_arr = [temp] * avg_readings
h_arr = [humidity] * avg_readings

##########################
###	 MAIN LOOP	  ###
##########################
while True:
	curr = time.time()
	dt = datetime_str()
	# Get current readings and clamp them to ranges, if necessary
	try:
		temp, humidity = get_readings(sensor_arduino.readline())
		mean_t, mean_h = average_th(temp, humidity)
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
	# Write the data to the display arduino and to firebase
	try:
		if changed:
			print "Changed: {} {}".format(set_temp, set_humidity)
		if curr > last_display + display_freq:
			last_display = curr
			display_q.put([send_to_display, mean_t, mean_h])
		if curr > last_push + push_freq:
			last_push = curr
			firebase_q.put([fb_push_th, mean_t, mean_h])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
	# Write the data to the sensor arduino
	try:
		colors = [scale_byte(r, max_r), scale_byte(g, max_g), scale_byte(b, max_b), 2]
		color = "RGB{0:0{3}x}{1:0{3}x}{2:0{3}x}".format(*colors)
		dc, tc, hc = "d0", "t0", "h0"
		ds, ts, hs = "OFF", "OFF", "OFF"
		if demo_mode:
			dc, ds = "d1", "ON"
		if mean_t < set_temp - t_fudge:
			tc, ts = "t1", "ON"
		if mean_h < set_humidity - h_fudge:
			hc, hs = "h1", "ON"
		sensor_q.put([send_to_sensor, dc, tc, hc, color])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
	# Log Everything that occurred
	try:
		if curr > last_out + out_freq:
			last_out = curr
			s = outstr.format(dt, temp, mean_t, set_temp, humidity, mean_h, set_humidity, r, g, b, ds, ts, hs)
			logging_q.put([log_string, logfile, s])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])
	# Push the logs to firebase if it's time
	try:
		if curr > last_log_update + log_freq:
			last_log_update = curr
			firebase_q.put([fb_push_log, dt_start, os.path.basename(logfile), logfile, "controller"])
	except Exception as e:
		logging_q.put([log_string, logfile, str(e)])

for thread in pool:
	del(thread)
